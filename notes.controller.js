const fs = require('fs/promises')
const path = require('path')
const chalk = require('chalk')

const notesPath = path.join(__dirname, 'db.json')

async function addNote(title) {
    const notes = await getNotes()

    const note = {
        title: title,
        id: Date.now().toString()
    }
    notes.push(note)

    await saveNotes(notes)
    console.log(chalk.bgGreen('Note added'))
}

async function getNotes() {
    const notes = await fs.readFile(notesPath, {encoding: 'utf8'})
    const parsed = JSON.parse(notes)
    return Array.isArray(parsed) ? parsed : []
}

async function printNotes() {
    const notes = await getNotes()
    console.log(chalk.bgBlue('List of Notes'))
    console.log(chalk.bgBlue('----------------'))
    notes.forEach(note => {
        console.log(chalk.blue(`${note.id}: ${note.title}`))
    })
    console.log(chalk.bgBlue('----------------'))
}

async function deleteNote(id) {
    const notes = await getNotes()
    const noteIndex = notes.findIndex(note => note.id === id)
    if (noteIndex === -1) {
        console.log(chalk.bgRed('Note not found'))
        return
    }
    notes.splice(noteIndex, 1)
    await saveNotes(notes)
    console.log(chalk.bgGreen('Note removed'))
}

async function saveNotes(notes) {
    await fs.writeFile(notesPath, JSON.stringify(notes))
}

module.exports ={
    addNote,
    printNotes,
    deleteNote
}
