const yargs = require('yargs');
const pkg = require('./package.json');
const {addNote, printNotes, deleteNote} = require('./notes.controller');

yargs.version('v' + pkg.version);

yargs.command({
    command: 'add',
    describe: 'Add a new note',
    builder: {
        title: {
            describe: 'Note title',
            type: 'string',
            demandOption: true
        }
    },
    handler({title}) {
        addNote(title)
    }
})

yargs.command({
    command: 'list',
    describe: 'Print all notes',
    async handler() {
        printNotes()
    }
})

yargs.command({
    command: 'remove',
    describe: 'Remove note by id',
    builder: {
        id: {
            describe: 'Id for remove',
            type: 'string',
            demandOption: true
        }
    },
    async handler({id}) {
        deleteNote(id)
    }
})



yargs.parse()
