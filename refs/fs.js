const fs = require('fs/promises')
const fsSync = require('fs')
const path = require('path')

const base = path.join(__dirname, 'temp')

const getContent = () => `\n\r${process.argv[2] ?? ''}`

async function main() {
     try {
         if (!fsSync.existsSync(base)) {
             await fs.mkdir(base)
             console.log('Directory created')
         }
         await fs.appendFile(path.join(base, 'logs.txt'), getContent())
         const data = await fs.readFile(path.join(base, 'logs.txt'), {encoding: 'utf8'})
         console.log(data)
     } catch (err) {
         console.log(err)
     }
}

main()
